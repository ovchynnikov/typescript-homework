import { http } from "./http";
import createMovieCardElements from "./movieCardCreator";
import clearLocalStorage from "./clearLocalStorage";
import removeMoviesFromDOM from "./removeMoviesFromDOM";
import Results from "./results";

export default async function upcomingButtonClick(): Promise<void> {

	clearLocalStorage();

	const upcomingList: Results = await http("upcoming");

	removeMoviesFromDOM("upcoming");

	createMovieCardElements(upcomingList.results, undefined);

}