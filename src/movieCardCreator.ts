import { Movie } from "./movie";

export default function createMovieCardElements(movies: Array<Movie>, favorites= false): void {

	const movieLength: number = movies.length;
	const filmContainer = document.getElementById("film-container");
	const favoriteContainer = document.getElementById("favorite-movies");

	const likedMoviesString: string | null = localStorage.getItem("likedMovies");
	const likedMovies  = likedMoviesString ? JSON.parse(likedMoviesString) : {};
	
	for (let i = 0; i < movieLength; i++) {	

    const divCol = document.createElement("div");
    divCol.className = favorites? "col-12 p-2" : "col-lg-3 col-md-4 col-12 p-2";
    
    const divCard = document.createElement("div");
    divCard.className = "card shadow-sm";
    
    const img = document.createElement("img");
    img.setAttribute("src", `https://image.tmdb.org/t/p/original/${movies[i].poster_path}`);
    
    const svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    svg.setAttributeNS(null, "class","bi bi-heart-fill position-absolute p-2 cursor");
    svg.setAttributeNS(null, "stroke","red");
		const fillColor = likedMovies[movies[i].id] ? 'red' : "#ff000078";
    svg.setAttributeNS(null, "fill", fillColor);
    svg.setAttributeNS(null, "width","50");
    svg.setAttributeNS(null, "height","50");
    svg.setAttributeNS(null, "viewBox","0 -2 18 22");
		
    const path = document.createElementNS('http://www.w3.org/2000/svg',"path");
		path.setAttributeNS(null, "id", `${movies[i].id}`);
		path.setAttributeNS(null, "class", "bi bi-heart-fill position-absolute p-2");
    path.setAttributeNS(null, "fill-rule","evenodd");
    path.setAttributeNS(null, "d","M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z");
    
    const divCardBody = document.createElement("div");
    divCardBody.className = "card-body";
    
    const p = document.createElement("p");
    p.className = "card-text truncate";
    p.textContent = movies[i].overview;
    
    const divInsideCardBody = document.createElement("div");
    divInsideCardBody.className = "d-flex justify-content-between align-items-center";
    
    const small = document.createElement("small");
    small.className = "text-muted";
    small.textContent = `${movies[i].release_date}` || '';
    
    divInsideCardBody.appendChild(small);
    
    divCardBody.appendChild(p);
    divCardBody.appendChild(divInsideCardBody);
    
    svg.appendChild(path);
    
    divCard.appendChild(img);
    divCard.appendChild(svg);
    divCard.appendChild(divCardBody);
    divCol.appendChild(divCard);
    
		if(favorites && favoriteContainer) favoriteContainer.appendChild(divCol);
    if (filmContainer && favorites === false) filmContainer.appendChild(divCol);
	}
}