export default function addMovieToFavourites(target: EventTarget | null): void {

	const reveivedTarget = target as HTMLElement;
	const targetParent = reveivedTarget.parentNode as HTMLElement ;
	const parentDiv = targetParent && targetParent.parentNode ? targetParent.parentNode.parentNode as HTMLElement : null;
	const favoritelistDiv = document.getElementById("favorite-movies");

	const likedMoviesString: string | null  = localStorage.getItem("likedMovies");

	interface likedMovies {
		[key: string]: string;
	}

	if (reveivedTarget.nodeName == 'path') {
		const likedMovies: likedMovies  = likedMoviesString ? JSON.parse(likedMoviesString) : {};
    const {id} = reveivedTarget;
    likedMovies[id] ? delete likedMovies[id] : likedMovies[id] = id;
    localStorage.setItem("likedMovies", JSON.stringify(likedMovies));
		const svgLike = reveivedTarget.parentNode as HTMLElement;
		const svgLikeFill = svgLike.getAttribute("fill");
		
		svgLikeFill === "red" ? svgLike.setAttributeNS(null, "fill","#ff000078") : svgLike.setAttributeNS(null, "fill","red");
	}
	
	const classMathed:boolean = parentDiv ? (parentDiv.classList.value === "col-12 p-2" ) : false;
	 
	if(parentDiv && classMathed) {
		if(favoritelistDiv) favoritelistDiv.removeChild(parentDiv);
	 }
}