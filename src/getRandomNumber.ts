export default function getRandomNumber(to: number):number {
	return Math.floor(Math.random() * to) + 1;
}