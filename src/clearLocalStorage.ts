export default function clearLocalStorage(): void {
	localStorage.removeItem('lastRequest');
	localStorage.removeItem('nextPage');
	localStorage.removeItem('lastSearch');

}