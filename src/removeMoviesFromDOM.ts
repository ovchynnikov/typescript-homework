export default async function removeMoviesFromDOM(domContainer: string): Promise<void>{

	const filmContainer = document.getElementById("film-container");
	const favoriteContainer = document.getElementById("favorite-movies");

	if (favoriteContainer && domContainer === 'favorites') favoriteContainer.innerHTML = '';
	if (filmContainer && (domContainer === 'popular' || domContainer === 'upcoming' || domContainer === 'top_rated' || domContainer === 'search')) filmContainer.innerHTML = '';
}