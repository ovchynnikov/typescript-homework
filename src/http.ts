import mapper from "./mapper";
import Results from "./results";

export async function http(request: string , page = 1, query:string | null= ''): Promise<Results> {
	
	const API_KEY = '08165637961852bba76cb227c6c1cdf3';
	const baseUrl: string = request === 'search' ? `https://api.themoviedb.org/3/search/movie` : `https://api.themoviedb.org/3/movie/`;
  const response = await fetch(request === 'search' && query ? `${baseUrl}?api_key=${API_KEY}&query=${query}&page=${page}` : `${baseUrl}${request}?api_key=${API_KEY}&page=${page}`);
	const body: Results = await response.json();

	const nextPage = '2';
	localStorage.setItem('lastRequest', request);
	localStorage.setItem('nextPage', nextPage);

	const mappedBody = await mapper(body);
	return mappedBody;
}