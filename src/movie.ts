export interface Movie {
	backdrop_path: string;
	id: number;
	overview: string;
	poster_path: string;
	release_date: Date;
	title: string;
	
}