import fetchMovieById from "./fetchMovieById";
import { Movie } from "./movie";
import createMovieCardElements from "./movieCardCreator";
import removeMoviesFromDOM from "./removeMoviesFromDOM";

export default async function favoriteListRender(): Promise<void> {

	const stringFromLocalStorage: string | null = localStorage.getItem("likedMovies");

	 interface FavoriteMoviesId {
     [key: string]: string;
	 }

	if(stringFromLocalStorage) {
	   const likedMovieList: FavoriteMoviesId = JSON.parse(stringFromLocalStorage) || {} ;
	   
	   removeMoviesFromDOM('favorites');

    const favoriteMoviesList: Movie[] = [];
      for (const [_key, value] of Object.entries(likedMovieList)) {
	     const movie: Movie = await fetchMovieById(value + '');
	     favoriteMoviesList.push(movie);
 		  }

    createMovieCardElements(favoriteMoviesList, true);
  }
}

