import clearLocalStorage from "./clearLocalStorage";
import { http } from "./http";
import createMovieCardElements from "./movieCardCreator";
import removeMoviesFromDOM from "./removeMoviesFromDOM";
import Results from "./results";

export default async function searchMovieButtonClick(): Promise<void> {
	
	clearLocalStorage();

	const searchInput = (<HTMLInputElement>document.getElementById("search"));
	const query: string = searchInput.value;

	if(query.length > 1) localStorage.setItem('lastSearch', query);
	const lastSearchCheck = localStorage.getItem('lastSearch');
	const lastSearch = lastSearchCheck && lastSearchCheck.length > 1 ? lastSearchCheck : '';

	if (query && query.length > 1) {

		const { results }: Results = await http("search", undefined ,query.length > 1 ? query : lastSearch);
		
		removeMoviesFromDOM("search");
		
		createMovieCardElements(results, undefined);
		if (searchInput) searchInput.value = '';
	}
}