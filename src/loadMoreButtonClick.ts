import { http } from "./http";
import createMovieCardElements from "./movieCardCreator";
import Results from "./results";

export default async function loadMoreButtonClick(): Promise<void> {

	const lastUploading: string | null = localStorage.getItem('lastRequest');
	let lastPage: string | null | number = localStorage.getItem('nextPage');
	const lastSearch: string | null = localStorage.getItem('lastSearch');


	if(lastUploading) {

		lastPage = lastPage ? lastPage : 1;

		const loadMoreList: Results = await http(lastUploading, +lastPage, lastSearch);
			
		createMovieCardElements(loadMoreList.results, undefined);

		const nextPage: string = (+lastPage+1).toString()
		localStorage.setItem('nextPage', nextPage);
	}

}