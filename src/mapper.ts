import Results from "./results";

export default function mapper(body: Results): Results {

	const dataForApp: Results = body;

	type MovieObj = typeof MovieObj;

	const MovieObj = {	
		backdrop_path: '',
		id: 1,
		overview: '',
		poster_path: '',
		title: '',
		release_date: ''
	};

	for (let i = 0; i < dataForApp.results.length; i++) {
		const movie: MovieObj = dataForApp.results[i];

		for (const key of Object.keys(movie)) {
			if( !(key in MovieObj) ) {
				delete dataForApp.results[i][key]
			}
		}
	}

	return dataForApp;
};