import { Movie } from './movie';
export default async function fetchMovieById(id: string): Promise<Movie> {

	const API_KEY = '08165637961852bba76cb227c6c1cdf3';
	const baseUrl = `https://api.themoviedb.org/3/movie/`
	const response = await fetch(`${baseUrl}${id}?api_key=${API_KEY}`);
	const movie: Movie = await response.json();

	return movie;
}