import { Movie } from './movie';


export default function setRandomMovie(movie: Movie):void {
	const baseUrl = `https://image.tmdb.org/t/p/original/`;

	const randomContainer = document.getElementById("random-movie");
	if(randomContainer) randomContainer.style.backgroundImage = `url(${baseUrl}${movie.backdrop_path})`;

	const randomMovieName = document.getElementById("random-movie-name");
	if(randomMovieName) randomMovieName.textContent = movie.title;

	const randomMovieDesc = document.getElementById("random-movie-description");
	if(randomMovieDesc) randomMovieDesc.textContent = movie.overview;
}