import popularButtonClick from "./popularButtonClick";
import searchMovieButtonClick from "./searchMovieButtonClick";
import topRatedButtonClick from "./topRatedButtonClick";
import upcomingButtonClick from "./upcomingButtonClick";

export default function updateLikes(): void {

	const lastUploading: string | null = localStorage.getItem('lastRequest');
	
switch (lastUploading) {
  case 'popular':
    popularButtonClick();
    break;
  case 'upcoming':
		upcomingButtonClick();
		break;
	case 'top_rated':
		topRatedButtonClick();
		break;
	case 'search':
		searchMovieButtonClick();
		break;
  default:
    popularButtonClick();
	}

}