import clearLocalStorage from "./clearLocalStorage";
import { http } from "./http";
import createMovieCardElements from "./movieCardCreator";
import removeMoviesFromDOM from "./removeMoviesFromDOM";
import Results from "./results";

export default async function topRatedButtonClick(): Promise<void> {

	clearLocalStorage();

	const topRatedList: Results = await http("top_rated");

	removeMoviesFromDOM("top_rated");

	createMovieCardElements(topRatedList.results, undefined);
}