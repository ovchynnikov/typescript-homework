import getRandomNumber from "./getRandomNumber";
import { http } from "./http";
import createMovieCardElements from "./movieCardCreator";
import setRandomMovieMovie from "./setRandomMovie";
import upcomingButtonClick from './upcomingButtonClick';
import popularButtonClick from './popularButtonClick';
import topRatedButtonClick from './topRatedButtonClick';
import loadMoreButtonClick from './loadMoreButtonClick';
import searchMovieButtonClick from "./searchMovieButtonClick";
import addMovieToFavourites from "./addMovieToFavourites";
import favoriteListRender from "./favoriteListRender";
import updateLikes from "./updateLikes";
import Results from "./results";


export async function render(): Promise<void> {
	
	const data: Results = await http('popular',undefined, undefined);
	const Movies = data.results;

	const MoviesLength: number = data.results.length;

  createMovieCardElements(Movies, undefined);
	setRandomMovieMovie(Movies[getRandomNumber(MoviesLength)]);

	const upcomingButton = document.getElementById('upcoming');
  if(upcomingButton) upcomingButton.addEventListener('click', upcomingButtonClick);

	const popularButton = document.getElementById('popular');
  if(popularButton) popularButton.addEventListener('click', popularButtonClick);

	const topRatedButton = document.getElementById('top_rated');
  if(topRatedButton) topRatedButton.addEventListener('click', topRatedButtonClick);

	const searchButton = document.getElementById('submit');
  if(searchButton) searchButton.addEventListener('click', searchMovieButtonClick);

	const loadMoreButton = document.getElementById('load-more');
  if(loadMoreButton) loadMoreButton.addEventListener('click', loadMoreButtonClick);

  if(document.body) {document.body.addEventListener('click', event => {
		addMovieToFavourites(event.target)
	})} 

	const openFavoritesButton = document.getElementsByClassName('navbar-toggler')[0];
	if(openFavoritesButton) openFavoritesButton.addEventListener('click', favoriteListRender);

	const favoritesSidebar = document.getElementById('offcanvasRight');
	if(favoritesSidebar) favoritesSidebar.addEventListener('blur', updateLikes); 

}