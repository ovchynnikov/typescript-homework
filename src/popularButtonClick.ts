import { http } from "./http";
import createMovieCardElements from "./movieCardCreator";
import removeMoviesFromDOM from "./removeMoviesFromDOM";
import Results from "./results";

export default async function popularButtonClick(): Promise<void> {
	
	localStorage.removeItem('nextPage');
	
	const popularList: Results = await http("popular");
	
	removeMoviesFromDOM("popular");

	createMovieCardElements(popularList.results, undefined);

}